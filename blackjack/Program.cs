﻿using System;
using loginRegister;
using game;

namespace blackjack
{
    class Program
    {
        static void Main(string[] args)
        {
            string pathToDatabase = AppDomain.CurrentDomain.BaseDirectory + "../../../database/userCredentials.txt";
            User loggedUser = LoginRegister.LoginMenu();
          

            Console.WriteLine("\nVitaj " + loggedUser.username + ". Na účte máš " + loggedUser.money + " eur.");
            Console.WriteLine("Pre začatie novej hry stlačte klávesu.");
            Console.ReadLine();
            Console.WriteLine("");
            
            // New game
            NewRound:
                loggedUser = Game.newGame(loggedUser);
                LoginRegister.onSaveUser(loggedUser, pathToDatabase);

                Console.WriteLine("\nNa účte vám zostalo: " + loggedUser.money + " eur.");

                ChooseAgain:
                Console.WriteLine("\nChcete odohrať dalšie kolo?");
                Console.WriteLine("1 - Áno");
                Console.WriteLine("2 - Nie");
                int choosenValue;
                while (!int.TryParse(Console.ReadLine(), out choosenValue))
                {
                    Console.WriteLine("\nNezadali ste spravnu hodnotu\n");
                    goto ChooseAgain;
                }

                // New  game
                if (choosenValue == 1)
                {
                    if(loggedUser.money < 10)
                    {
                        LoginRegister.resetAccount(loggedUser);
                    }
                    goto NewRound;
                }

                // On logout and exit game
                else if (choosenValue == 2)
                {
                    Console.WriteLine("Ďakujeme za hru. Dnes ste skončili so zostatkom na účte " + loggedUser.money + " eur. Dovidenia");
                    LoginRegister.onSaveUser(loggedUser, pathToDatabase);
                }

                // Invalid value
                else
                {
                    goto ChooseAgain;
                }
        }
    }
}
