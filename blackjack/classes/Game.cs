﻿using System;
using cards;
using System.Collections.Generic;
using loginRegister;

namespace game
{
    class Game
    {

        public static User newGame(User loggedUser)
        {
            
            ChooseInvestment:
            Console.WriteLine("\nKolko peňazí chcete staviť? (na vasom ucte je " + loggedUser.money + " eur)");

            int n;
            while (!int.TryParse(Console.ReadLine(), out n))
            {
                goto ChooseInvestment;
            }

                int investedMoney = Convert.ToInt32(n);

                if (investedMoney > loggedUser.money) {
                    Console.WriteLine("Na účte nemáte toľko peňazí. Zadajte menej než " + loggedUser.money);
                    goto ChooseInvestment;
                } else if (investedMoney < 10)
                {
                    Console.WriteLine("Minimálna investícia je 10 eur. ");
                    goto ChooseInvestment;
                }

            List<string> cardPack       = Cards.getNewCardPack();
            List<string> dealersCards   = new List<string>();
            List<string> playersCards   = new List<string>();

            int playersCardSum          = 0;
            int dealersCardSum          = 0;
            Random random               = new Random();

            // Starting cards
            for (int i = 0; i < 2; i++)
            {
                int randomNumber = random.Next(cardPack.Count);
                dealersCards.Add(cardPack[randomNumber]);
                dealersCardSum = _calculateDealersCardSum(dealersCardSum, cardPack[randomNumber]);
                cardPack.RemoveAt(randomNumber);

                randomNumber = random.Next(cardPack.Count);
                playersCards.Add(cardPack[randomNumber]);
                playersCardSum = _calculatePlayersCardSum(playersCardSum, cardPack[randomNumber]);
                cardPack.RemoveAt(randomNumber);
            }

            Console.WriteLine("Dealerove karty sú: " + dealersCards[0] + " a *");
            Console.WriteLine("Vaše karty sú: " + playersCards[0] + " a " + playersCards[1]);

            NextMove:
                bool nextMove = _nextPlayersMove();

                // PlayerMove
                if (nextMove == true)
                {
                    
                    int randomNumber    = random.Next(cardPack.Count);
                    playersCards.Add(cardPack[randomNumber]);

                    Console.WriteLine("\nPotiahli ste si kartu " + cardPack[randomNumber] + "\n");

                    _displayCurrentCards(playersCards);
   
                    playersCardSum = _calculatePlayersCardSum(playersCardSum, cardPack[randomNumber]);
                    cardPack.RemoveAt(randomNumber);

                    //Player lost
                    if (playersCardSum > 21)
                    {
                        Console.WriteLine("\nHodnota vašich kariet je viac ako 21. Prehrali ste!");
                        loggedUser.money -= investedMoney;
                    }
                    else
                    {
                        goto NextMove;
                    }
                }

            // Dealer move
            else
            {
                dealersCardSum = _dealerMove(dealersCardSum, cardPack);

                if (dealersCardSum > 21)
                {
                    Console.WriteLine("\nVyhrali ste! Hodnota dealerovych kariet presiahla 21.");
                    loggedUser.money += investedMoney;
                }
                else if (dealersCardSum == playersCardSum)
                {
                    Console.WriteLine("\nRemíza! Hodnota vašich kariet bola " + playersCardSum);
                }
                else if (dealersCardSum > playersCardSum)
                {
                    Console.WriteLine("\nPrehrali ste! Hodnota vašich kariet je " + playersCardSum + " a hodnota dealerových kariet je " + dealersCardSum + ".");
                    loggedUser.money -= investedMoney;
                }
                else
                {
                    Console.WriteLine("\nVyhrali ste! Hodnota vašich kariet je " + playersCardSum + " a hodnota dealerových kariet je " + dealersCardSum + ".");
                    loggedUser.money += investedMoney;
                }
            }

            return loggedUser;
        }

        public static int _dealerMove(int dealersCardSum, List<string> cardPack)
        {
            Random random    = new Random();
            int randomPlay   = random.Next(8);

            if (dealersCardSum >= 21)
            {
                return dealersCardSum;

            }
            else if (dealersCardSum < 15)
            {
                int randomNumber    = random.Next(cardPack.Count);
                dealersCardSum      = _calculateDealersCardSum(dealersCardSum, cardPack[randomNumber]);
                cardPack.RemoveAt(randomNumber);

                // If dealers cards sum is less than 15, make another move
                if (dealersCardSum < 15)
                {
                    dealersCardSum = _dealerMove(dealersCardSum, cardPack);

                    return dealersCardSum;
                }
            }
            // Random risky moves
            else if (randomPlay == 2 && dealersCardSum < 18)
            {
                int randomNumber    = random.Next(cardPack.Count);
                dealersCardSum      = _calculateDealersCardSum(dealersCardSum, cardPack[randomNumber]);

                cardPack.RemoveAt(randomNumber);
            }
            else if (randomPlay == 6 && dealersCardSum < 16)
            {
                int randomNumber    = random.Next(cardPack.Count);
                dealersCardSum      = _calculateDealersCardSum(dealersCardSum, cardPack[randomNumber]);

                cardPack.RemoveAt(randomNumber);
            }

            else
            {
                return dealersCardSum;
            }

            // NEEDS TO BE FIXED - If removed, function returns error that not all paths return value
            return dealersCardSum;
        }

        public static bool _nextPlayersMove()
        {
            NextMove:
                Console.WriteLine("\nChcete si ťahať dalšiu kartu?");
                Console.WriteLine("1 - Áno");
                Console.WriteLine("2 - Nie");

                int choosenNumber;
                while (!int.TryParse(Console.ReadLine(), out choosenNumber))
                {
                    Console.WriteLine("\nNezadali ste spravnu hodnotu\n");
                    goto NextMove;
                }

                if (choosenNumber == 1)
                {
                    return true;
                }
                // NEEDS TO BE FIXED - Why is else redundant?
                else if (choosenNumber == 2)
                {
                    return false;
                }
                else
                {
                    Console.WriteLine("\nNezadali ste spravnu hodnotu\n");
                    goto NextMove;
                }
        }

        public static int _calculateDealersCardSum(int dealersCardSum, string card)
        {
            if (card == "A" && dealersCardSum <= 10)
            {
                dealersCardSum += 11;
            }
            else if (card == "A" && dealersCardSum > 10)
            {
                dealersCardSum += 1;
            }
            else if (card == "J" || card == "Q" || card == "K")
            {
                dealersCardSum += 10;
            }
            else
            {
                dealersCardSum += Convert.ToInt32(card);
            }

            return dealersCardSum;
        }

        public static int _calculatePlayersCardSum(int playersCardSum, string card)
        {
            if (card == "A")
            {
                ChooseAgain:
                    Console.WriteLine("\nPotiahli ste si kartu A, chcete aby mala hodnotu 1 alebo 11?");

                    int value;
                    while (!int.TryParse(Console.ReadLine(), out value))
                    {
                        Console.WriteLine("\nNezadali ste spravnu hodnotu\n");
                        goto ChooseAgain;
                    }

                    if (value == 1 || value == 11)
                    {
                        playersCardSum += value;
                    } else
                    {
                        goto ChooseAgain;
                    }
            }
            else if (card == "J" || card == "Q" || card == "K")
            {
                playersCardSum += 10;
            }
            else
            {
                playersCardSum += Convert.ToInt32(card);
            }

            return playersCardSum;
        }
        
        public static void _displayCurrentCards(List<string> playersCards)
        {
            Console.Write("Vaše aktuálne karty sú: ");
            foreach (string card in playersCards)
            {
                Console.Write(card + " ");
            }
            Console.WriteLine("");
        }
    }
}
