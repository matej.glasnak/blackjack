﻿using System;
using System.IO;

namespace loginRegister
{
    class LoginRegister
    {
        public static User LoginMenu()
        {
            Console.WriteLine("Vitajte!");
            string pathToDatabase = AppDomain.CurrentDomain.BaseDirectory + "../../../database/userCredentials.txt";

            User loggedUser;

            // Login or register
            ChooseLoginOrRegister:
                Console.Write("1. Prihlásiť sa\n2. Zaregistorvať sa\n");
                Console.Write("Ktorú možnosť si želáte zvoliť? ");
                int loginRegister;
                while (!int.TryParse(Console.ReadLine(), out loginRegister))
                {
                    Console.WriteLine("Nezadali ste cislo, zadaj celociselnu hodnotu: ");
                }

                if (loginRegister == 1)
                {
                    loggedUser = _onLoginUser(pathToDatabase);
                }
                else if (loginRegister == 2)
                {
                    loggedUser = _onRegisterUser(pathToDatabase);
                }
                else
                {
                    Console.WriteLine("Taka moznost nie je, skuste znova..\n");
                    goto ChooseLoginOrRegister;
                }

            return loggedUser;
        }

        private static User _onRegisterUser(string pathToDatabase)
        {
            Register:
                Console.Write("\nZadajte meno, pod ktorym chcete byt zaregistrovany: ");
                var username = Console.ReadLine();
                if (string.IsNullOrEmpty(username))
                {
                    goto Register;
                }
                File.AppendAllText(pathToDatabase, username + '&' + 300 + '/');
                Console.WriteLine("Registrácia prebehla úspešne, teraz sa môžete prihlásiť. ");
                User loggedUser = _onLoginUser(pathToDatabase);
                return loggedUser;
        }

        private static User _onLoginUser(string pathToDatabase)
        {
            Login:
                Console.Write("\nZadajte prihlasovacie meno: ");
                string username         = Console.ReadLine();
                if (string.IsNullOrEmpty(username))
                {
                    goto Login;
                }

                bool valid              = false;
                int money               = 0;

                string credentials      = File.ReadAllText(pathToDatabase);
                string[] userProfiles   = credentials.Split('/');

                foreach (string userProfile in userProfiles)
                {

                    string[] singleUser = userProfile.Split('&');

                    if (singleUser[0] == username)
                    {
                        valid = true;
                        money = Convert.ToInt32(singleUser[1]);
                        break;
                    }

                }

                if (valid == false)
                {
                    Console.WriteLine("Používateľ nebol nájdený, skúste to znova.");
                    goto Login;
                }

            User loggedUser = new User(username, money);

            if (loggedUser.money < 10)
            {
                resetAccount(loggedUser);
            }

            Console.WriteLine("Boli ste úspešne prihlásený");

            return loggedUser;
        }

        public static void onSaveUser(User loggedUser, string pathToDatabase)
        {
            string credentials      = File.ReadAllText(pathToDatabase);
            string userCredentials  = "";

            string[] userProfiles   = credentials.Split('/');

            foreach (string userProfile in userProfiles)
            {
                string[] singleUser = userProfile.Split('&');

                if (singleUser[0] == loggedUser.username)
                {
                    userCredentials += loggedUser.username + "&" + loggedUser.money + "/";

                }
                else
                {
                    // If userProfile is not the last empty element that was created while splitting by /
                    if (userProfile.Length >= 1)
                    {
                        userCredentials += userProfile + "/";
                    }
                }

                File.WriteAllText(@pathToDatabase, userCredentials);
            }
        }

        public static void resetAccount(User loggedUser)
        {
            {
            ResetAccount:
                Console.WriteLine("\nNa vasom ucte mate " + loggedUser.money + " eur. S takou hodnotou nemozte dalej vsadzat. Chcete resetovat ucet?\n1 - Ano\n2 - Nie");
                int resetacc;
                while (!int.TryParse(Console.ReadLine(), out resetacc))
                {
                    Console.WriteLine("Nezadali ste spravnu volbu");
                }
                if (resetacc == 1)
                {
                    loggedUser.money = 300;
                }
                else if (resetacc == 2)
                {
                    Console.WriteLine("\nOkej, vypinam hru.");
                    System.Environment.Exit(1);
                }
                else
                {
                    goto ResetAccount;
                }
            }
        }
    }
}
