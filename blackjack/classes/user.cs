﻿class User
{
    public string username;
    public int money;

    public User(string username, int money)
    {
        this.username = username;
        this.money = money;
    }
}